module TuringBelt (Alphabet, Belt, movel, mover, bset, bget) where
	import Data.Sequence
	data Alphabet = Alphabet [Char] Char
	data Belt = Belt (Seq Char) Alphabet Int

	movel :: Belt -> Belt
	movel (Belt se al 0) = Belt (end <| se) al 0 where (Alphabet _ end) = al
	movel (Belt a b i) = Belt a b (i - 1)

	mover :: Belt -> Belt
	mover (Belt a b i) = if
			 (i == Data.Sequence.length a) then
			 Belt (end <| a) b (i+1)
			 else
			 Belt a b (i + 1)
			 where (Alphabet _ end) = b

	bget :: Belt -> Maybe Char
	bget (Belt s _ i) = Data.Sequence.lookup i s

	bset :: Belt -> Char -> Belt
	bset (Belt s a i) c = Belt (Data.Sequence.update i c s) a i


